\select@language {italian}
\select@language {italian}
\contentsline {chapter}{Introduzione}{1}{chapter*.2}
\contentsline {chapter}{\numberline {1}Analisi dati Monte Titoli S.p.A}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Caricamento dati in R}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Nomi delle variabili}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Analisi della struttura dati}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Valori nulli}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Simboli significativi}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Esecuzione di un grafico tramite funzione plot()}{4}{section.1.6}
\contentsline {section}{\numberline {1.7}Esecuzione di un grafico tramite pacchetto ggplot2()}{4}{section.1.7}
\contentsline {chapter}{\numberline {2}Creazione di file PDF, HTML, WORD}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Generazione dei file}{6}{section.2.1}
\contentsline {chapter}{\numberline {3}Presentazione Markdown Presentation}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Presentazione HTML}{7}{section.3.1}
\contentsline {chapter}{Bibliografia}{8}{section.3.1}
