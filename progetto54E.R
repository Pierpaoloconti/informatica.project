library(Quandl)
library(ggplot2)
library(xts)
library(zoo)
dati<-Quandl("ECB/SST_A_IT_MTI_D01_Z_Z0Z_Q", authcode="oiguchmzWpJufKapN5zr")
write.csv(dati,file = 'Progetto54E.csv')
dati <- read.csv('Progetto54E.csv',header=TRUE,sep=',',stringsAsFactors=FALSE)
simboli <- names(dati) #determinazione simboli nella variabile dati
str(dati) #analizzo struttura dati 
summary(dati) #riporto le principali statistiche della variabile dati
for (i in 1:length(simboli)) {
  print(summary(dati[simboli[i]])) 
}  

#Controllo se presenti dati NA
dati.buoni <- complete.cases(dati)
dati.na <- FALSE
print(nrow(dati))
print(nrow(dati[dati.buoni,]))
if (nrow(dati) == nrow(dati[dati.buoni,])) {
   print('Non ci sono NA')
} else {
    dati.na <- TRUE
    print('Ci sono NA')
}

#creare il nuovo dati
dati.m <- NULL
if (dati.na) {
  dati.m <- dati[dati.buoni,]
} else {
  dati.m = dati
}

date.m <- as.Date(dati.m$Date, "%Y-%m-%d") 

#recuperare qualche dato statistico
for (i in 1:length(simboli)) {
  if (!is.character(dati.m[,simboli[i]])) {
    msg <- c('Simbolo:', min(dati.m[,simboli[i]]))
    print(simboli[i])
    msg <- c('min', min(dati.m[,simboli[i]]))
    print(msg)
    msg <- c('max', max(dati.m[,simboli[i]]))
    print(msg)
    msg <- c('mean', mean(dati.m[,simboli[i]]))
    print(msg)
    summary(dati.m[,simboli[i]])
  }
}  

#Grafico tra valori min,max,mean e creazione immagine in formato png
plot(date.m,dati.m[,simboli[3]])
dev.copy(png,filename="figura54E.1.png",width=880,height=480)
dev.off()
abline(h=min(dati.m[,simboli[3]]))
abline(h=max(dati.m[,simboli[3]]))
#Stampo grafico di abline min,max
dev.copy(png,filename="figura54E.2.png",width=880,height=480)
dev.off()

# grafico tra le date gestite come factor e i valori in Value
plot(as.factor(dati.m[,simboli[2]]),dati.m[,simboli[3]])

# aggiungere la colonna anno (Year)
dati.m$Year <- format(date.m,'%Y')
# aggiungere la colonna mese (Month)
dati.m$Month <- format(date.m,'%m')

# grafico considerando gli anni e i valori
plot(dati.m$Year,dati.m$Value)
unique(dati.m$Year)

#grafico ottenuto con pacchetto ggplot2
ggplot(dati.m, aes(x=Pure.number,y=dati.m[,simboli[3]], fill = Year)) +
  geom_bar(stat="identity") +
  facet_wrap(~Year) +
  theme(legend.title=element_blank(),
        legend.position = "none")
dev.copy(png,filename="figura54E.3.png",width=880,height=480)
dev.off()

