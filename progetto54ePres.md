progetto54E - Presentazione
========================================================
author: Pierpaolo Conti
date: 17/06/2016

========================================================
Carico i dati dal file **progetto54E.csv** nel formato csv, ottenuto una volta scaricati i dati con la funzione **Quandl()**.

```r
dati <- read.csv('progetto54e.csv',header=TRUE,sep=',',stringsAsFactors=FALSE)
```
========================================================
Recupero le informazioni sui simboli (sulle variabili) del data frame nella variabile **dati**.

```r
simboli <- names(dati)
simboli
```

```
[1] "X"           "Date"        "Pure.number"
```
========================================================
Ottengo le informazioni sulla struttura dei dati.

```r
str(dati)
```

```
'data.frame':	10 obs. of  3 variables:
 $ X          : int  1 2 3 4 5 6 7 8 9 10
 $ Date       : chr  "2014-12-31" "2013-12-31" "2012-12-31" "2011-12-31" ...
 $ Pure.number: int  6 7 7 7 6 4 4 2 2 2
```
========================================================
Recupero qualche informazione statistica sui dati caricati.

- Sull'intero data frame

```r
summary(dati) #intero data frame
```

```
       X             Date            Pure.number  
 Min.   : 1.00   Length:10          Min.   :2.00  
 1st Qu.: 3.25   Class :character   1st Qu.:2.50  
 Median : 5.50   Mode  :character   Median :5.00  
 Mean   : 5.50                      Mean   :4.70  
 3rd Qu.: 7.75                      3rd Qu.:6.75  
 Max.   :10.00                      Max.   :7.00  
```
========================================================

# Modifica del data frame iniziale
Verifico se ci sono degli NA nel data frame **dati**.

```r
dati.buoni <- complete.cases(dati)
dati.na <- FALSE
print(nrow(dati))
```

```
[1] 10
```

```r
print(nrow(dati[dati.buoni,]))
```

```
[1] 10
```

```r
if (nrow(dati) == nrow(dati[dati.buoni,])) {
  print('Non ci sono NA')
} else {
  dati.na <- TRUE
  print('Ci sono NA')
}
```

```
[1] "Non ci sono NA"
```
========================================================
Creo il nuovo data frame a partire da quello contenuto nella variabile **dati**.

```r
dati.m <- NULL
if (dati.na) {
  dati.m <- dati[dati.buoni,]
} else {
  dati.m = dati
}
```
========================================================
Definisco una nuova variabile **date.m** che prende i valori della variabile **Date** del data frame **dati.m** nel formato **Date**.

```r
date.m <- as.Date(dati.m$Date, "%Y-%m-%d") 
```
========================================================
Recupero qualche dato statistico sulle singole variabili del data frame **dati.m**.

```r
for (i in 1:length(simboli)) {
  if (!is.character(dati.m[,simboli[i]])) {
    msg <- c('Simbolo:', min(dati.m[,simboli[i]]))
    print(simboli[i])
    msg <- c('min', min(dati.m[,simboli[i]]))
    print(msg)
    msg <- c('max', max(dati.m[,simboli[i]]))
    print(msg)
    msg <- c('mean', mean(dati.m[,simboli[i]]))
    print(msg)
    summary(dati.m[,simboli[i]])
  }
}  
```

```
[1] "X"
[1] "min" "1"  
[1] "max" "10" 
[1] "mean" "5.5" 
[1] "Pure.number"
[1] "min" "2"  
[1] "max" "7"  
[1] "mean" "4.7" 
```
========================================================
# Rappresentazione grafica

Realizzo un grafico mettendo:

- sull'asse delle ascisse i valori delle date della variabile **date.m**;
- sull'asse delle ordinate i dati della variabile **Value** del data frame **dati.m**.

```r
plot(date.m,dati.m[,simboli[3]], col='red')
abline(h=min(dati.m[,simboli[3]]),col='blue')
abline(h=max(dati.m[,simboli[3]]),col='green')
abline(h=mean(dati.m[,simboli[3]]),col='black')
```

![plot of chunk unnamed-chunk-9](progetto54ePres-figure/unnamed-chunk-9-1.png) 
========================================================
Realizzo un grafico mettendo:
- sull'asse delle ascisse i valori delle date della variabile **Date** del data frame **dati.m** convertita da character a factor;
- sull'asse delle ordinate i dati della variabile **Value** del data frame **dati.m**.
========================================================
In aggiunta le etichette sull'asse delle ascisse sono ruotate di 90 gradi. Per posizionarle correttamente sul grafico ho settato in modo opportuno i parametri **srt** e **pos** della funzione **text()**.

```r
ascisse <- as.factor(dati.m[,simboli[2]]) 
plot(ascisse,dati.m[,simboli[3]],xaxt="n")
lablist <- as.vector(dati.m[,simboli[2]])
axis(1, at=ascisse, labels = FALSE)
text(x=ascisse, y=par("usr")[3] - 0.2, labels = lablist, srt = 90, pos = 2.7, xpd = TRUE)
```

![plot of chunk unnamed-chunk-10](progetto54ePres-figure/unnamed-chunk-10-1.png) 
========================================================
Estratti dalla variabile **date.m** i dati relativi all'anno (Year) e al mese (Month), definendo variabili corrispondenti nel data frame **dati.m**.

Per far questo � sufficiente utilizzare la funzione **format()**, applicata ad una variabile **date.m** di tipo **Date**.

```r
dati.m$Year <- format(date.m,'%Y')
dati.m$Month <- format(date.m,'%m')
names(dati.m)
```

```
[1] "X"           "Date"        "Pure.number" "Year"        "Month"      
```
========================================================
Realizzo un grafico mettendo:

- sull'asse delle ascisse i valori della variabile **Year** del data frame **dati.m**;
- sull'asse delle ordinate i dati della variabile **Value** del data frame **dati.m**.

Inoltre applico la funzione **unique()** alla variabile **Year** del data frame **dati.m** per mostrare gli anni di riferimento presenti nel data frame. 

```r
plot(dati.m$Year,dati.m$Value)
```

![plot of chunk unnamed-chunk-12](progetto54ePres-figure/unnamed-chunk-12-1.png) 

```r
unique(dati.m$Year)
```

```
 [1] "2014" "2013" "2012" "2011" "2010" "2009" "2008" "2007" "2006" "2005"
```
========================================================

Concludo realizzando un grafico tramite funzioni della libreria **ggplot2**.


```r
library(ggplot2)
ggplot(dati.m, aes(x=Pure.number,y=dati.m[,simboli[3]], fill = Year)) +
  geom_bar(stat="identity") +
  facet_wrap(~Year) +
  theme(legend.title=element_blank(),
        legend.position = "none")
```

![plot of chunk unnamed-chunk-13](progetto54ePres-figure/unnamed-chunk-13-1.png) 
