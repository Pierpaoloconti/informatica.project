\select@language {italian}
\select@language {italian}
\contentsline {chapter}{Introduzione}{1}{chapter*.2}
\contentsline {chapter}{\numberline {1}Analisi dati Monte Titoli S.p.A}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Caricamento librerie}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Caricamento dati in R}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Nomi delle variabili}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}Analisi della struttura dati}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Valori nulli}{3}{section.1.5}
\contentsline {section}{\numberline {1.6}Simboli significativi}{4}{section.1.6}
\contentsline {section}{\numberline {1.7}Esecuzione di un grafico tramite funzione plot()}{4}{section.1.7}
\contentsline {section}{\numberline {1.8}Esecuzione di un grafico tramite pacchetto ggplot2()}{5}{section.1.8}
\contentsline {chapter}{\numberline {2}Creazione di file PDF, HTML, WORD}{7}{chapter.2}
\contentsline {section}{\numberline {2.1}Generazione dei file}{7}{section.2.1}
\contentsline {chapter}{\numberline {3}Presentazione Markdown Presentation}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Presentazione HTML}{8}{section.3.1}
\contentsline {chapter}{Bibliografia}{9}{section.3.1}
